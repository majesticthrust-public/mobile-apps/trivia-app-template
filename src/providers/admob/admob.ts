import { Injectable } from "@angular/core";

import { AdMobFreeRewardVideoConfig } from "@ionic-native/admob-free";

import { config } from "../../app/app.config";

@Injectable()
export class AdmobProvider {
  constructor(
  ) {
  }

  public getRewardVideoConfig(): AdMobFreeRewardVideoConfig {
    // TODO pass geolocation

    return {
      id: config.admob.rewardedVideoId,
      isTesting: config.admob.isTesting,
      autoShow: false,
      forChild: config.admob.forChild,
      forFamily: config.admob.forFamily
    } as AdMobFreeRewardVideoConfig;
  }
}
