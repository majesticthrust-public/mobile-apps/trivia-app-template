import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import "rxjs/add/operator/map";


interface QuizResponseQuestion {
  category: string;
  type: string;
  difficulty: string;
  question: string;
  correct_answer: string;
  incorrect_answers: string[];
}

interface QuizResponse {
  response_code: number;
  results: QuizResponseQuestion[];
}

export interface Question {
  category: string;
  type: string;
  difficulty: string;
  question: string;
  correct_answer: string;
  answers: string[];
}

export type Quiz = Question[];

export interface QuestionResult {
  correctAnswer: string;
  givenAnswer: string;
}

/**
 * Состояние игры; объект с состоянием передается между страницами через navParams
 */
export interface QuizGameState {
  quiz: Quiz;
  currentIndex: number;
  results: QuestionResult[];
}

export interface Category {
  id: string;
  name: string;
}

interface CategoryLookupResponse {
  trivia_categories: Category[];
}

function decodeText(text: string) {
  return (new DOMParser).parseFromString(text, "text/html").documentElement.textContent;
}

/**
 * Fisher-Yates shuffle. Shuffles in place. Returns shuffled array.
 * @param arr array to shuffle
 */
function shuffle(arr) {
  let currentIndex = arr.length, temporaryValue, randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = arr[currentIndex];
    arr[currentIndex] = arr[randomIndex];
    arr[randomIndex] = temporaryValue;
  }
  return arr;
}

@Injectable()
export class QuizProvider {
  url: string;
  apiEndpoint: string;
  categoryLookupEndpoint: string;
  categoryQuestionCountLookupEndpoint: string;
  globalQuestionCountLookupEndpoint: string;

  constructor(public http: HttpClient) {
    this.url = "https://opentdb.com";
    this.apiEndpoint = "/api.php";
    this.categoryLookupEndpoint = "/api_category.php";
    this.categoryQuestionCountLookupEndpoint = "/api_count.php";
    this.globalQuestionCountLookupEndpoint = "/api_count_global.php";
  }

  onInit() {

  }

  getCategories() {
    const url = this.url + this.categoryLookupEndpoint;
    return this.http.get<CategoryLookupResponse>(url)
      .map(response => response.trivia_categories)
      .toPromise();
  }

  getQuiz(category: number, difficulty: string, amount: number) {
    const url = this.url + this.apiEndpoint;

    return this.http.get<QuizResponse>(url, {
      params: {
        amount: amount.toString(),
        category: category.toString(),
        difficulty
      }
    })
      // decode response text
      .map<QuizResponse, Quiz>((response) => {
        return response.results.map((question) => {
          const correct_answer = decodeText(question.correct_answer);
          const answers = [...question.incorrect_answers.map(decodeText), correct_answer];

          return <Question>{
            category: question.category,
            type: question.type,
            difficulty: question.difficulty,
            question: decodeText(question.question),
            correct_answer,
            answers
          };
        });
      })

      .toPromise();
  }

  /**
   * Создает начальное состояние игры из викторины. Перемешивает викторину.
   * @param quiz викторина
   */
  initGame(quiz: Quiz): QuizGameState {
    // shuffle questions
    const shuffledQuiz = shuffle(quiz);

    // shuffle answers
    shuffledQuiz.forEach((question) => {
      shuffle(question.answers);
    });

    return {
      quiz: shuffledQuiz,
      currentIndex: 0,
      results: []
    };
  }

  /**
   * Обнуляет состояние игры. Перемешивает викторину.
   * @param gameState старое состояние игры
   */
  resetGame(gameState: QuizGameState): QuizGameState {
    return Object.assign(gameState, this.initGame(gameState.quiz));
  }
}
