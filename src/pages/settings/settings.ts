import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { Category, QuizProvider } from "../../providers/quiz/quiz";

export interface QuizOptions {
  category: number;
  difficulty: string;
  amount: number;
}

@IonicPage()
@Component({
  selector: "page-settings",
  templateUrl: "settings.html",
})
export class SettingsPage {
  options: QuizOptions;
  categories: Category[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private quizProvider: QuizProvider,
    private storage: Storage
  ) {
    this.options = {
      category: 17,
      difficulty: "medium",
      amount: 10
    };
  }

  async ionViewWillEnter() {
    this.categories = await this.quizProvider.getCategories();

    const options: QuizOptions = await this.storage.get("options");
    console.log("Options get", options);

    // overwrite defaults with data from db
    Object.assign(this.options, options);
  }

  async saveForm() {
    await this.storage.set("options", this.options);
    console.log("Options set", this.options);
    this.navCtrl.parent.select(0);
  }
}
