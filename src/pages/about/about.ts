import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { config } from "../../app/app.config";

@Component({
  selector: "page-about",
  templateUrl: "about.html"
})
export class AboutPage {
  about: {
    version?: string,
    name?: string,
    description?: string,
    author?: string
  };

  constructor(public navCtrl: NavController) {
    this.about = Object.assign({}, config.about);
  }
}
