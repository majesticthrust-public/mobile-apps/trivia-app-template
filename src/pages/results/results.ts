import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { GameViewPage } from "../game-view/game-view";
import { HomePage } from "../home/home";
import { QuizGameState, QuizProvider } from "../../providers/quiz/quiz";

@IonicPage()
@Component({
  selector: "page-results",
  templateUrl: "results.html",
})
export class ResultsPage {
  gameState: QuizGameState;
  results: { question: string, givenAnswer: string, isCorrect: boolean }[];
  quizTotal: number;
  correctTotal: number;
  scoreType: "perfect" | "good" | "bad";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public quizProvider: QuizProvider
  ) {
    this.gameState = navParams.get("gameState");
  }

  ionViewWillEnter() {
    // количество вопросов и правильных ответов
    const { results } = this.gameState;
    const answers = results.map(({ correctAnswer, givenAnswer }) => correctAnswer === givenAnswer);
    this.quizTotal = answers.length;
    this.correctTotal = answers.filter(a => a).length;

    const ratio = this.correctTotal / this.quizTotal;

    if (ratio == 1) {
      this.scoreType = "perfect";
    }
    else if (ratio >= 0.75) {
      this.scoreType = "good";
    }
    else {
      this.scoreType = "bad";
    }

    // вопросы с данными ответами
    this.results = [];
    for (let i = 0; i < this.gameState.results.length; i++) {
      this.results.push({
        question: this.gameState.quiz[i].question,
        givenAnswer: this.gameState.results[i].givenAnswer,
        isCorrect: answers[i]
      });
    }
  }

  backHome() {
    this.navCtrl.setRoot(HomePage);
  }

  restart() {
    const blankState = this.quizProvider.resetGame(this.gameState);
    this.navCtrl.setRoot(GameViewPage, { gameState: blankState });
  }

}
