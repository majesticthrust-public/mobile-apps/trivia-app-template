import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { QuizProvider, QuizGameState, Quiz } from "../../providers/quiz/quiz";
import { Storage } from "@ionic/storage";
import { GameViewPage } from "../game-view/game-view";
import { QuizOptions } from "../settings/settings";


@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  gameState: QuizGameState;
  isQuizLoaded: boolean = false;
  errorOccured: boolean = false;

  constructor(
    public navCtrl: NavController,
    private quizProvider: QuizProvider,
    private storage: Storage
  ) { }

  async ionViewWillEnter() {
    let options: QuizOptions = await this.storage.get("options");

    if (options === null) {
      options = {
        category: 15,
        difficulty: "medium",
        amount: 10
      };
    }

    const { category, difficulty, amount } = options;
    let quiz: Quiz = [];

    try {
      quiz = await this.quizProvider.getQuiz(category, difficulty, amount);
      this.isQuizLoaded = true;
    }
    catch (e) {
      console.error("Couldn't get a quiz from opentdb", e);
      this.errorOccured = true;
    }

    console.log("Downloaded quiz: ", quiz);
    this.gameState = this.quizProvider.initGame(quiz);
  }

  start() {
    this.navCtrl.setRoot(GameViewPage, { gameState: this.gameState });
  }
}
