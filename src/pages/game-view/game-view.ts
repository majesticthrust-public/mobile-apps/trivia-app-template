import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FeedbackPage } from "../feedback/feedback";
import { ResultsPage } from "../results/results";
import { QuizGameState, Question } from "../../providers/quiz/quiz";
import { AdMobFree } from "@ionic-native/admob-free";

@IonicPage()
@Component({
  selector: "page-game-view",
  templateUrl: "game-view.html",
})
export class GameViewPage {
  gameState: QuizGameState;
  currentQuestion: Question;
  isRewardedVideoReady: boolean = false;
  isAnswerRevealed: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private admob: AdMobFree
  ) {
    this.gameState = this.navParams.get("gameState");
  }

  async ionViewWillEnter() {
    const { currentIndex, quiz } = this.gameState;

    // готовим вопрос или завершаем викторину
    if (currentIndex < quiz.length) {
      this.currentQuestion = quiz[currentIndex];
    } else {
      this.navCtrl.setRoot(ResultsPage, { gameState: this.gameState });
    }

    this.isRewardedVideoReady = await this.admob.rewardVideo.isReady();
  }

  handleAnswer(answer) {
    this.gameState.results.push({
      correctAnswer: this.currentQuestion.correct_answer,
      givenAnswer: answer
    });

    this.navCtrl.setRoot(FeedbackPage, { gameState: this.gameState });
  }

  async revealAnswer() {
    document.addEventListener(this.admob.events.REWARD_VIDEO_REWARD, (e) => {
      // reveal the answer
      // may fire multiple times, but it doesn't matter, so no need to fix
      this.isAnswerRevealed = true;
      console.log("Reward!!!111");
    }, { once: true });

    try {
      await this.admob.rewardVideo.show();
    }
    catch {
      console.log("Whoops, something went wrong during the attempt to show the ad.");
    }
  }
}
