import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { GameViewPage } from "../game-view/game-view";
import { QuizGameState, QuestionResult } from "../../providers/quiz/quiz";

@IonicPage()
@Component({
  selector: "page-feedback",
  templateUrl: "feedback.html",
})
export class FeedbackPage {
  gameState: QuizGameState;
  questionResult: QuestionResult;
  isAnswerCorrect: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.gameState = navParams.get("gameState");
  }

  async ionViewWillEnter() {
    const { currentIndex, results } = this.gameState;
    const result = results[currentIndex];

    this.questionResult = result;
    this.isAnswerCorrect = result.correctAnswer === result.givenAnswer;
  }

  nextQuestion() {
    this.gameState.currentIndex++;
    this.navCtrl.setRoot(GameViewPage, { gameState: this.gameState });
  }
}
