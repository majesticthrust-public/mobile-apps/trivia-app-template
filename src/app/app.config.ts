export const config = {
  admob: {
    isTesting: true,
    rewardedVideoId: "",
    forChild: false,
    forFamily: false,
    useLocation: true
  },

  about: {
    version: "1.0.0",
    name: "",
    description: "",
    author: ""
  }
};
