import { Component } from "@angular/core";
import { Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { TabsPage } from "../pages/tabs/tabs";
import { AdMobFree } from "@ionic-native/admob-free";
import { AdmobProvider } from "../providers/admob/admob";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  rootPage: any = TabsPage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    admob: AdMobFree,
    admobProvider: AdmobProvider
  ) {
    platform.ready().then(() => {
      statusBar.hide();
      splashScreen.hide();

      admob.rewardVideo.config(admobProvider.getRewardVideoConfig());
      admob.rewardVideo.prepare();

      // prepare after every close
      document.addEventListener(admob.events.REWARD_VIDEO_CLOSE, (e) => admob.rewardVideo.prepare());

      // log events
      for (const ev of Object.keys(admob.events)) {
        document.addEventListener(admob.events[ev], console.log);
      }
    });
  }
}
