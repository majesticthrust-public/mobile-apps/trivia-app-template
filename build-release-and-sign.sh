#!/bin/bash

PROJECT_ROOT="."
RELEASE_APK="$PROJECT_ROOT/platforms/android/build/outputs/apk/android-release-unsigned.apk"
KEYSTORE_FILE="$PROJECT_ROOT/release-key.jks"
KEYSTORE_ALIAS="app-release-key"
OUTPUT_DIR="$PROJECT_ROOT/output"
OUTPUT_APK="$OUTPUT_DIR/release-signed.apk"

# apksigner - это скрипт, который является батником на винде и шелл-скриптом на юниксах
# предположим, что содержащая его папка есть в PATH, и попробуем узнать, какой вариант скрипта есть
# найденному скрипту сделаем alias
# если не найдем, то будем использовать jarsigner
APKSIGNER_EXISTS=true
if command -v apksigner > /dev/null 2>&1 ; then 
    APKSIGNER_CMD=apksigner
elif command -v apksigner.bat > /dev/null 2>&1 ; then
    APKSIGNER_CMD=apksigner.bat
else
    echo "apksigner not found! Falling back to jarsigner" >&2
    APKSIGNER_EXISTS=false
fi

# проверяем на ошибки и в случае возникновения таковых завершаемся с ошибкой
function exit_if_error {
    if [ $? -ne 0 ]; then
        echo Error has occured >&2
        exit 1
    fi
}

# создадим keystore, если он не существует
if [ ! -f "$KEYSTORE_FILE" ]; then
    echo "Generating keystore"
    keytool -genkey -v -keystore $KEYSTORE_FILE -keyalg RSA -keysize 2048 -validity 10000 -alias $KEYSTORE_ALIAS
    exit_if_error
fi

# соберем apk
ionic cordova build android --prod --release
exit_if_error

# создадим/почистим папку для выходных файлов
if [ -d output ]; then
    echo "Cleaning output dir"
    rm -rfv $OUTPUT_DIR/*
else
    echo "Creating output dir"
    mkdir $OUTPUT_DIR
fi
exit_if_error

if [ "$APKSIGNER_EXISTS" = false ]; then
    # подпишем apk jarsigner-ом
    echo "Signing with jarsigner"
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE_FILE $RELEASE_APK $KEYSTORE_ALIAS
    exit_if_error
fi

# сделаем zipalign и высрем apk туда, куда надо
echo "Doing zipalign"
zipalign -v 4 $RELEASE_APK $OUTPUT_APK
exit_if_error

if [ "$APKSIGNER_EXISTS" = true ]; then
    # подпишем apk apksigner-ом
    echo "Signing with apksigner"
    $APKSIGNER_CMD sign --ks $KEYSTORE_FILE --ks-key-alias $KEYSTORE_ALIAS $OUTPUT_APK
    exit_if_error

    # проверим, что apk действительно подписан
    echo "Verifying signature with apksigner"
    $APKSIGNER_CMD verify $OUTPUT_APK
    exit_if_error
fi